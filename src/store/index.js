import { createStore } from 'vuex'

export default createStore({
  state: {
    contador: 10
  },
  mutations: {
    subirContador(state, payload) {
      state.contador = state.contador + payload
    },
    reducirContador(state, payload) {
      state.contador = state.contador - payload
    }
  },
  actions: {
    redContador({ commit }, numero) {
      commit('reducirContador', numero)
    },
    accionEstado({ commit }, object){
      object.estado ? commit('subirContador', object.numero) : commit('reducirContador', object.numero)
    }
  },
  modules: {
  }
})
